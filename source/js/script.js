// Подключение js-файлов с помощью rigger-а

// Vendor
//= vendor/imask.js
//= vendor/modernizr-custom.js
//= vendor/swiper.js

// Modules
//= modules/getSlider.js
//= modules/getTabToggle.js
//= modules/openHeaderMobile.js
//= modules/catalog.js
//= modules/formMask.js
//= modules/formTelHover.js
//= modules/formLocalStorage.js
//= modules/formSubmit.js
